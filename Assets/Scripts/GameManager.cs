﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public GameObject player;
    public int lives;
    public int enemies;

    public Texture2D livesTexture;

    public GUIStyle guiStyle;

	// Use this for initialization
	void Start () 
    {
        //enemies = 8;
        lives = 10;
        player = GameObject.Find("Player");
        enemies = GameObject.FindGameObjectsWithTag("Enemy").Length;
	}
	
	// Update is called once per frame
	void Update () 
    {
        //enemies = GameObject.FindGameObjectsWithTag("Enemy").Length;
	}

    void OnGUI()
    {
        //Draws the lives texture, top left.
        GUI.Label(new Rect(10, 5, 50, 50), livesTexture);
        //Draws the remaining lives, top left.
        GUI.Label(new Rect(60, 0, 200, 200), " X " + lives, guiStyle);
        //Draws the remaining enemies, top right.
        GUI.Label(new Rect(900, 5, 200, 200), enemies.ToString(), guiStyle);
    }
}
