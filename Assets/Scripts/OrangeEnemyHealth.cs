﻿using UnityEngine;
using System.Collections;

public class OrangeEnemyHealth : MonoBehaviour {

    public Sprite sprite0;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;

    private SpriteRenderer spriteRenderer;

    OrangeEnemyMovement orangeEnemyMovement;

	// Use this for initialization
	void Start () 
    {
        orangeEnemyMovement = GetComponentInParent<OrangeEnemyMovement>();

        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer.sprite == null)
        {
            spriteRenderer.sprite = sprite3;
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        ChangeSprite();
	}

    void ChangeSprite()
    {
        if (orangeEnemyMovement.health >= 3)
        {
            spriteRenderer.sprite = sprite3;
        }

        if (orangeEnemyMovement.health == 2)
        {
            spriteRenderer.sprite = sprite2;
        }

        if (orangeEnemyMovement.health == 1)
        {
            spriteRenderer.sprite = sprite1;
        }

        if (orangeEnemyMovement.health <= 0)
        {
            spriteRenderer.sprite = sprite0;
        }
    }
}
