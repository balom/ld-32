﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    public float horizontal;
    public float vertical;
    public float speed;
    public float health;
    public bool facingRight;
    public static Animator anim;
    bool canAttack = true;
    public Transform playerHealthTransform;
    public GameObject playerRespawnPrefab;
    GameManager gameManager;
    Respawn respawn;

	// Use this for initialization
	void Start () 
    {
        anim = GetComponent<Animator>();
        speed = 75;
        health = 3;
        facingRight = true;
        // Similar to "GameObject.Find", except it searches only children and
        // returns a "Transform" rather than a "GameObject"
        playerHealthTransform = transform.Find("PlayerHealth");
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        respawn = GameObject.Find("GameManager").GetComponent<Respawn>();
        gameManager.player = gameObject;
	}
	
	// Update is called once per frame
	void Update () 
    {
        Movement();
        Attacks();
	}

    void Movement()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        rigidbody2D.velocity = new Vector2(horizontal * speed, vertical * speed);
        if (horizontal < 0 && facingRight)
        {
            facingRight = false;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            playerHealthTransform.localScale = new Vector2(-playerHealthTransform.localScale.x, playerHealthTransform.localScale.y);
        }
        else if (horizontal > 0 && !facingRight)
        {
            facingRight = true;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            playerHealthTransform.localScale = new Vector2(-playerHealthTransform.localScale.x, playerHealthTransform.localScale.y);
        }

        if (horizontal != 0 || vertical != 0)
        {
            anim.SetBool("Walking", true);
        }
        else
        {
            anim.SetBool("Walking", false);
        }
    }

    void Attacks()
    { 
        if (Input.GetButtonDown(buttonName: "Fire1"))
        {
            if (canAttack)
            {
                StartCoroutine(AttackingCoroutine());
            }
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            TakeDamage(1, coll.transform.position);
        }
    }

    void TakeDamage(int damage, Vector3 attackerPosition)
    {
        int knockbackForce = 2000;
        Vector3 forceDirection = transform.position - attackerPosition;
        rigidbody2D.AddForce(forceDirection * knockbackForce);
        health -= damage;
        if (health <= 0)
        {
            Die();
            
        }
    }

    void Die()
    {
        Instantiate(playerRespawnPrefab, new Vector3(0, 0, 0), transform.rotation);
        respawn.dead = true;
        gameManager.lives--;
        Destroy(gameObject);
    }

    IEnumerator AttackingCoroutine()
    {
        canAttack = false;
        anim.SetBool("Attacking", true);
        yield return new WaitForSeconds(0.6f);
        anim.SetBool("Attacking", false);
        yield return new WaitForSeconds(0.2f);
        canAttack = true;
    }
}
