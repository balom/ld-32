﻿using UnityEngine;
using System.Collections;

public class OrangeEnemyMovement : MonoBehaviour {

    public float speed;
    public float health;
    GameObject player;
    public bool facingRight;
    public Transform orangeEnemyHealthTransform;
    public GameManager gameManager;
    public int direction;

	// Use this for initialization
	void Start () 
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        player = gameManager.player;
        speed = 25;
        health = 3;
        facingRight = false;
        orangeEnemyHealthTransform = transform.Find("OrangeEnemyHealth");
        direction = -1;
	}
	
	// Update is called once per frame
	void Update () 
    {
        Movement();
	}

    void Movement()
    {
        rigidbody2D.velocity = new Vector2(direction * speed, 0);
        if (rigidbody2D.velocity.x < 0 && facingRight)
        {
            facingRight = false;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            orangeEnemyHealthTransform.localScale = new Vector2(-orangeEnemyHealthTransform.localScale.x, orangeEnemyHealthTransform.localScale.y);
        }
        else if (rigidbody2D.velocity.x > 0 && !facingRight)
        {
            facingRight = true;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            orangeEnemyHealthTransform.localScale = new Vector2(-orangeEnemyHealthTransform.localScale.x, orangeEnemyHealthTransform.localScale.y);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Wall")
        {
            ReverseDirection();
        }
    }

    void ReverseDirection()
    {
        direction *= -1;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Weapon")
        {
            TakeDamage(1, coll.transform.position);
        }
    }

    void TakeDamage(int damage, Vector3 attackerPosition)
    {
        int knockbackForce = 2000;
        Vector3 forceDirection = transform.position - attackerPosition;
        rigidbody2D.AddForce(forceDirection * knockbackForce);
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }
        void Die()
    {
        gameManager.enemies--;
        Destroy(gameObject);
    }
}
