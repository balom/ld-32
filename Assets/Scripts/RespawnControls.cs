﻿using UnityEngine;
using System.Collections;

public class RespawnControls : MonoBehaviour {

    public float horizontal;
    public float vertical;
    public float speed;
    public bool canSpawn;
    Respawn respawn;

	// Use this for initialization
	void Start () 
    {
        speed = 150;
        canSpawn = true;
        respawn = GameObject.Find("GameManager").GetComponent<Respawn>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Movement();
	}

    void Movement()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        rigidbody2D.velocity = new Vector2(horizontal * speed, vertical * speed);
        if (Input.GetButtonDown("Fire2"))
        {
            if (canSpawn)
            {
                respawn.Resp(transform);
                Destroy(gameObject);
            }
        }
    }


    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Enemy")
        {
            canSpawn = false;
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.tag == "Enemy")
        {
            canSpawn = false;
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Enemy")
        {
            canSpawn = true;
        }
    }

}
