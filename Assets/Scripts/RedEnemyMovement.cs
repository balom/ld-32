﻿using UnityEngine;
using System.Collections;

public class RedEnemyMovement : MonoBehaviour {

    public float speed;
    public float health;
    public Vector3 targetDirection;
    GameObject player;
    public bool stunned;
    public bool facingRight;
    public bool flocking;
    public bool canGetNewDirection;
    public Transform redEnemyHealthTransform;
    public GameManager gameManager;

	// Use this for initialization
	void Start () 
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        //player = GameObject.FindGameObjectWithTag("Player");
        player = gameManager.player;
        speed = 30;
        health = 3;
        stunned = false;
        facingRight = false;
        canGetNewDirection = true;
        redEnemyHealthTransform = transform.Find("RedEnemyHealth");
	}
	
	// Update is called once per frame
	void Update () 
    {
        player = gameManager.player;
        Movement();
	}

    void Movement()
    {
        if (player != null)
        {
            targetDirection = player.transform.position - transform.position;
        }
        else if (player == null)
        {
            if (canGetNewDirection)
            {
                StartCoroutine(Flock());
            }
        }
        if (stunned == false)
        {
            rigidbody2D.velocity = targetDirection.normalized * speed;
        }
        else if (stunned == true)
        {
            rigidbody2D.velocity = new Vector3(0, 0, 0);
        }

        if (rigidbody2D.velocity.x < 0 && facingRight)
        {
            facingRight = false;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            redEnemyHealthTransform.localScale = new Vector2(-redEnemyHealthTransform.localScale.x, redEnemyHealthTransform.localScale.y);
        }
        else if(rigidbody2D.velocity.x > 0 && !facingRight)
        {
            facingRight = true;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            redEnemyHealthTransform.localScale = new Vector2(-redEnemyHealthTransform.localScale.x, redEnemyHealthTransform.localScale.y);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Weapon")
        {
            TakeDamage(1, coll.transform.position);
        }
    }

    void TakeDamage(int damage, Vector3 attackerPosition)
    {
        int knockbackForce = 2000;
        Vector3 forceDirection = transform.position - attackerPosition;
        rigidbody2D.AddForce(forceDirection * knockbackForce);
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
        StartCoroutine(Stunned());
    }

    IEnumerator Stunned()
    {
        stunned = true;
        yield return new WaitForSeconds(1f);
        stunned = false;
    }

    IEnumerator Flock()
    {
        canGetNewDirection = false;
        targetDirection = new Vector2(Random.Range(-200, 200), Random.Range(-200, 200));
        yield return new WaitForSeconds(Random.Range(1, 4));
        if (player != null)
        {
            flocking = false;
        }
        canGetNewDirection = true;
    }

    void Die()
    {
        gameManager.enemies--;
        Destroy(gameObject);
    }
}
