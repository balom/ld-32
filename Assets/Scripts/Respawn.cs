﻿using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour {

    public GameObject player;
    public GameObject playerPrefab;
    GameManager gameManager;
    public bool dead;

	// Use this for initialization
	void Start () 
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        player = GameObject.Find("Player");
        dead = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
        
	}
    public void Resp(Transform respawnTrans)
    {
        if (player == null && dead == true)
        {
            if (Input.GetButtonDown("Fire2"))
            {
                Instantiate(playerPrefab, new Vector3(respawnTrans.position.x, respawnTrans.position.y, respawnTrans.position.z), transform.rotation);
                dead = false;
            }
        }
    }
}
