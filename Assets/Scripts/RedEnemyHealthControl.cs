﻿using UnityEngine;
using System.Collections;

public class RedEnemyHealthControl : MonoBehaviour {

    public Sprite sprite0;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;

    private SpriteRenderer spriteRenderer;

    RedEnemyMovement redEnemyMovement;

	// Use this for initialization
	void Start () 
    {
        redEnemyMovement = GetComponentInParent<RedEnemyMovement>();

        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer.sprite == null)
        {
            spriteRenderer.sprite = sprite3;
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        ChangeSprite();
	}

    void ChangeSprite()
    {
        if (redEnemyMovement.health >= 3)
        {
            spriteRenderer.sprite = sprite3;
        }

        if (redEnemyMovement.health == 2)
        {
            spriteRenderer.sprite = sprite2;
        }

        if (redEnemyMovement.health == 1)
        {
            spriteRenderer.sprite = sprite1;
        }

        if (redEnemyMovement.health <= 0)
        {
            spriteRenderer.sprite = sprite0;
        }
    }
}
