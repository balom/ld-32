﻿using UnityEngine;
using System.Collections;

public class PlayerHealthControl : MonoBehaviour {

    public Sprite sprite0;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;

    private SpriteRenderer spriteRenderer;

    PlayerMovement playerMovement;

	// Use this for initialization
	void Start () 
    {
        playerMovement = GetComponentInParent<PlayerMovement>();

        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer.sprite == null)
        {
            spriteRenderer.sprite = sprite3;
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        ChangeSprite();
	}

    void ChangeSprite()
    {
        if (playerMovement.health >= 3)
        {
            spriteRenderer.sprite = sprite3;
        }

        if (playerMovement.health == 2)
        {
            spriteRenderer.sprite = sprite2;
        }
        
        if (playerMovement.health == 1)
        {
            spriteRenderer.sprite = sprite1;
        }

        if (playerMovement.health <= 0)
        {
            spriteRenderer.sprite = sprite0;
        }
    }
}
